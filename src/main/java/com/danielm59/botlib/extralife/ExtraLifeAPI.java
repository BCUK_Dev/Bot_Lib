package com.danielm59.botlib.extralife;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class ExtraLifeAPI
{
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private static BufferedReader request(URL url) throws Exception
    {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        switch (conn.getResponseCode())
        {
            case 200:
                return new BufferedReader(new InputStreamReader((conn.getInputStream())));
            case 304:
                //no change
                return null;
            default:
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " From:" + url.toString());
        }
    }

    static Team getTeamData(int teamID)
    {
        try
        {
            URL url = new URL(String.format("https://www.extra-life.org/api/teams/%d", teamID));
            BufferedReader br = request(url);
            String output;

            if (br != null && (output = br.readLine()) != null)
            {
                return gson.fromJson(output, Team.class);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;

    }

}
