package com.danielm59.botlib.discord;

import com.danielm59.botlib.commands.discord.DiscordCommandRegistry;
import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.games.slot.SlotGame;
import com.danielm59.botlib.games.trivia.TriviaGame;
import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.EventDispatcher;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.Role;
import discord4j.core.object.entity.User;
import discord4j.core.object.presence.Activity;
import discord4j.core.object.presence.Presence;
import discord4j.core.object.util.Snowflake;
import reactor.core.publisher.Mono;

public class DiscordBot
{
    public static DiscordClient discordClient;

    public void init()
    {
        registerDefaultModules();
        discordClient = getClient();
        EventDispatcher dispatcher = discordClient.getEventDispatcher();
        dispatcher.on(MessageCreateEvent.class).flatMap(event -> DiscordEvents.onMessageReceived(event)
                .onErrorResume(t ->
                {
                    System.out.println("Error running a command");
                    return Mono.empty();
                }))
                .subscribe();
        discordClient.login().block();
    }

    private void registerDefaultModules()
    {
        SlotGame slotGame = new SlotGame();
        DiscordEvents.registerCommands(slotGame);
        TriviaGame triviaGame = new TriviaGame();
        DiscordEvents.registerCommands(triviaGame);
    }

    public void registerModule(DiscordCommandRegistry module)
    {
        DiscordEvents.registerCommands(module);
    }

    private DiscordClient getClient()
    { // Returns an instance of the Discord client
        return new DiscordClientBuilder(Config.getInstance().getDiscordToken()).build();
    }

    public static MessageChannel getMessageChannel(String channelID)
    {
        return discordClient.getChannelById(Snowflake.of(channelID)).cast(MessageChannel.class).block();
    }

    public static Mono<Message> getMessage(Snowflake channelID, Snowflake messageID)
    {
        return discordClient.getMessageById(channelID, messageID);
    }

    public static Role getRole(Snowflake guildID, Long roleID)
    {
        return discordClient.getRoleById(guildID, Snowflake.of(roleID)).block();
    }

    public void setPlaying(String playing)
    {
        if (!playing.equals(""))
        {
            discordClient.updatePresence(Presence.online(Activity.listening(playing)));
        } else
        {
            discordClient.updatePresence(Presence.online());
        }
    }

    public Message getMessage(long channelID, long messageID)
    {
        return discordClient.getMessageById(Snowflake.of(channelID), Snowflake.of(messageID)).block();
    }

    public User getUser(Long discordID)
    {
        return discordClient.getUserById(Snowflake.of(discordID)).block();
    }
}