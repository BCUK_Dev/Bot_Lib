package com.danielm59.botlib.discord;

import com.danielm59.botlib.commands.discord.DiscordCommandRegistry;
import com.danielm59.botlib.commands.discord.DiscordCommands;
import com.danielm59.botlib.discord.pointSystem.PointSystem;
import com.danielm59.botlib.twitch.SFX;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import reactor.core.publisher.Mono;

public class DiscordEvents
{
    private static DiscordCommands commands = new DiscordCommands();

    public static Mono<Void> onMessageReceived(MessageCreateEvent event)
    {
        Message message = event.getMessage();

        String[] command = {""};
        if (message.getContent().isPresent())
            command = message.getContent().get().split(" ", 2);

        if (message.getAuthor().isPresent() && !message.getAuthor().get().isBot())
        {
            message.getAuthorAsMember().doOnSuccess(PointSystem::addXP).onErrorStop().block();
            //TODO: make this take the event
            SFX.play(command[0]);
            return commands.processCommand(event);
        }

        return Mono.empty();
    }

    public static void registerCommands(DiscordCommandRegistry module)
    {
        module.registerCommands(commands);
    }
}