package com.danielm59.botlib.discord.pointSystem;

import com.danielm59.botlib.configs.ConfigHandler;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.spec.EmbedCreateSpec;
import reactor.core.publisher.Mono;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class PointSystem
{
    private static final int pointsMin = 15;
    private static final int pointsMax = 25;

    private static final Random RNG = new Random();

    private static PointData pointData = new PointData();
    private static PointsRewards pointsRewards = new PointsRewards();

    public static void init()
    {
        pointData = ConfigHandler.loadJson(pointData);
        pointsRewards = ConfigHandler.loadJson(pointsRewards);
    }

    public static void addXP(Member user)
    {
        long time = System.currentTimeMillis();
        if(user != null)
        {
            long userID = user.getId().asLong();
            if (!pointData.containsKey(userID))
            {
                pointData.put(userID, pointData.new Member());
            }
            PointData.Member member = pointData.get(userID);
            member.name = user.getUsername(); //Keep name up to date as much as possible to make manual edits easy
            //Anti spam check
            if (time - member.lastXPTime > TimeUnit.MINUTES.toMillis(1))
            {
                member.lastXPTime = time;
                int points = RNG.nextInt(pointsMax - pointsMin + 1) + pointsMin;
                member.points += points;
                pointsRewards.process(user, member.points);
                ConfigHandler.saveJson(pointData);
            }
        }
    }

    public static Mono<Void> points(MessageCreateEvent event)
    {
        if (event.getMember().isPresent())
        {
            Member member = event.getMember().get();
            PointData.Member pointsMember = pointData.get(member.getId().asLong());

            Consumer<EmbedCreateSpec> embed = spec ->
            {
                spec.setAuthor(member.getDisplayName(), "", member.getAvatarUrl());
                spec.addField("Rank", String.format("%d/%d", pointData.getRank(member), pointData.size()), true);
                spec.addField("points", String.format("%d", pointsMember.points), true);
            };

            return event.getMessage().getChannel()
                    .flatMap(channel -> channel.createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(embed))).then();
        }
        return Mono.empty().then();
    }

}
