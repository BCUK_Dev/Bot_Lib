package com.danielm59.botlib.discord.pointSystem;

import com.danielm59.botlib.configs.JsonData;
import com.danielm59.botlib.discord.DiscordBot;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Role;

import java.io.File;
import java.util.TreeMap;

public class PointsRewards extends TreeMap<Integer, Long> implements JsonData
{
    private static final transient File rewardData = new File("PointsRewards.json");

    void process(Member user, int points)
    {
        if (this.size() > 0)
        {
            int rewardLevel = this.floorKey(points);
            if (this.containsKey(rewardLevel))
            {
                Role reward = DiscordBot.getRole(user.getGuildId(), this.get(rewardLevel));
                user.addRole(reward.getId(), "Reward");
            }
        }
    }

    @Override
    public File getFile()
    {
        return rewardData;
    }
}
