package com.danielm59.botlib.discord.music;

import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.playback.MutableAudioFrame;

import java.nio.ByteBuffer;

/**
 * This is a wrapper around AudioPlayer which makes it behave as an IAudioProvider for D4J. As D4J calls canProvide
 * before every call to provide(), we pull the frame in canProvide() and use the frame we already pulled in
 * provide().
 */
public class AudioProvider extends discord4j.voice.AudioProvider
{
    private final AudioPlayer audioPlayer;
    private final MutableAudioFrame frame = new MutableAudioFrame();

    /**
     * @param audioPlayer Audio player to wrap.
     */
    AudioProvider(final AudioPlayer audioPlayer)
    {
        super(ByteBuffer.allocate(StandardAudioDataFormats.DISCORD_OPUS.maximumChunkSize()));
        frame.setBuffer(getBuffer());
        this.audioPlayer = audioPlayer;
    }

    @Override
    public boolean provide()
    {
        final boolean didProvide = audioPlayer.provide(frame);
        if (didProvide)
        {
            getBuffer().flip();
        }
        return didProvide;
    }
}
