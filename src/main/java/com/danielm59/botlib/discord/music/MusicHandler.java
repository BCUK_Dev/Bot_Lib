package com.danielm59.botlib.discord.music;

import com.danielm59.botlib.Utils;
import com.danielm59.botlib.configs.MusicConfig;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.apache.commons.lang3.ArrayUtils;
import org.kitteh.irc.client.library.feature.twitch.event.WhisperEvent;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

public class MusicHandler
{
    private static AudioPlayerManager playerManager;
    private static AudioPlayer player;
    private static discord4j.voice.AudioProvider provider;
    private static TrackScheduler scheduler;

    static
    {
        playerManager = new DefaultAudioPlayerManager();
        playerManager.getConfiguration().setFrameBufferFactory(NonAllocatingAudioFrameBuffer::new);
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
        player = playerManager.createPlayer();
        player.setVolume(MusicConfig.getInstance().getVolume());
        scheduler = new TrackScheduler(player);
        provider = new AudioProvider(player);
    }

    public static discord4j.voice.AudioProvider getProvider()
    {
        return provider;
    }

    public static TrackScheduler getScheduler()
    {
        return scheduler;
    }

    public static Mono<Void> listTracks(MessageCreateEvent event, int page)
    {
        int length = scheduler.getPlaylist().size();
        int pages = length / 10 + (length % 10 > 0 ? 1 : 0);
        page = Math.min(page, pages);
        if (length > 0)
        {
            StringBuilder s = new StringBuilder();
            AudioTrack[] tracks = new AudioTrack[]{};
            tracks = ArrayUtils.subarray(scheduler.getPlaylist().toArray(tracks), (page - 1) * 10, page * 10);
            int i = 1;
            s.append(String.format("Page %d of %d\n", page, pages));
            for (AudioTrack track : tracks)
            {
                s.append(String.format("[%d] ", (page - 1) * 10 + i++));
                s.append(track.getInfo().title);
                s.append("\n");
            }
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage(s.toString())).then();
        } else
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("No Songs in playlist")).then();
        }
    }

    public static void loadAndPlay(MessageCreateEvent event, final String trackUrl)
    {
        playerManager.loadItemOrdered(player, trackUrl, new AudioLoadResultHandler()
        {
            @Override
            public void trackLoaded(AudioTrack track)
            {
                event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Adding to queue: " + track.getInfo().title)).subscribe();
                if (event.getMember().isPresent())
                    track.setUserData(event.getMember().get().getDisplayName());
                scheduler.queue(track);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist)
            {
                List<AudioTrack> tracks = playlist.getTracks();
                if (playlist.getName().startsWith("Search results for:"))
                {
                    AudioTrack track = tracks.get(0);
                    if (event.getMember().isPresent())
                        track.setUserData(event.getMember().get().getDisplayName());
                    scheduler.queue(track);
                    event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Adding to queue: " + track.getInfo().title)).subscribe();
                } else
                {
                    Collections.shuffle(tracks);
                    for (AudioTrack track : tracks)
                    {
                        if (event.getMember().isPresent())
                            track.setUserData(event.getMember().get().getDisplayName());
                        scheduler.queue(track);
                    }
                    event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Adding playlist to queue: " + playlist.getName())).subscribe();
                }
            }

            @Override
            public void noMatches()
            {
                event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Nothing found by " + trackUrl)).subscribe();
            }

            @Override
            public void loadFailed(FriendlyException exception)
            {
                event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Could not play: " + exception.getMessage())).subscribe();
            }
        });
    }

    public static void loadAndPlayPriority(final String trackUrl)
    {
        playerManager.loadItemOrdered(player, trackUrl, new AudioLoadResultHandler()
        {
            @Override
            public void trackLoaded(AudioTrack track)
            {
                scheduler.playPriority(track);
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist)
            {
                new Exception("SFX tried to play as playlist").printStackTrace();
            }

            @Override
            public void noMatches()
            {
                new Exception("Missing SFX").printStackTrace();
            }

            @Override
            public void loadFailed(FriendlyException exception)
            {
                exception.printStackTrace();
            }
        });
    }

    public static Mono<Void> setVolume(MessageCreateEvent event, String volume)
    {
        int vol = Utils.getInt(volume);
        if (vol >= 0 && vol <= 100)
        {
            player.setVolume(Integer.parseInt(volume));
            MusicConfig.getInstance().setVolume(Integer.parseInt(volume));
            return getVolume(event);
        } else
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Please enter a number between 0 and 100")).then();
        }
    }

    public static void setVolume(WhisperEvent event, String volume)
    {
        int vol = Utils.getInt(volume);
        if (vol >= 0 && vol <= 100)
        {
            player.setVolume(Integer.parseInt(volume));
            MusicConfig.getInstance().setVolume(Integer.parseInt(volume));
            getVolume(event);
        } else
        {
            event.sendReply("Please enter a number between 0 and 100");
        }
    }

    public static Mono<Void> getVolume(MessageCreateEvent event)
    {
        return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Volume set to " + player.getVolume())).then();
    }

    public static void getVolume(WhisperEvent event)
    {
         event.sendReply("Volume set to " + player.getVolume());
    }

    public static Mono<Void> togglePause(MessageCreateEvent event)
    {
        player.setPaused(!player.isPaused());

        if (player.isPaused())
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Music Paused")).then();
        } else
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Music Resumed")).then();
        }
    }

    public static void togglePause(WhisperEvent event)
    {
        player.setPaused(!player.isPaused());

        if (player.isPaused())
        {
            event.sendReply("Music Paused");
        } else
        {
            event.sendReply("Music Resumed");
        }
    }
}
