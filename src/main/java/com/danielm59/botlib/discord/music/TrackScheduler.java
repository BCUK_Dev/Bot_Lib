package com.danielm59.botlib.discord.music;

import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.configs.MusicConfig;
import com.danielm59.botlib.configs.SFXfiles;
import com.danielm59.botlib.discord.DiscordBot;
import com.danielm59.botlib.twitch.LiveStreams;
import com.danielm59.botlib.twitch.irc.IRCBot;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import discord4j.core.object.presence.Activity;
import discord4j.core.object.presence.Presence;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * This class schedules tracks for the audio player. It contains the queue of tracks.
 */
public class TrackScheduler extends AudioEventAdapter
{
    private final AudioPlayer player;
    private final LinkedBlockingDeque<AudioTrack> queue;
    boolean loop = false;
    private boolean sfx = false;
    private boolean resume = false;

    TrackScheduler(final AudioPlayer player)
    {
        this.player = player;
        player.addListener(this);
        this.queue = new LinkedBlockingDeque<>();
    }

    void queue(AudioTrack track)
    {
        // Calling startTrack with the noInterrupt set to true will start the track only if nothing is currently playing. If
        // something is playing, it returns false and does nothing. In that case the player was already playing so this
        // track goes to the queue instead.
        if (!player.startTrack(track, true))
        {
            queue.offer(track);
        }
    }

    void playPriority(AudioTrack track)
    {
        player.setPaused(true);
        if (player.getPlayingTrack() != null)
        {
            AudioTrack clone = player.getPlayingTrack().makeClone();
            clone.setPosition(player.getPlayingTrack().getPosition());
            queue.offerFirst(clone);
            resume = true;
        }
        queue.offerFirst(track);
        player.setVolume(SFXfiles.getInstance().getVolume());
        sfx = true;
        nextTrack();
        player.setPaused(false);
    }

    public void clear()
    {
        queue.clear();
    }

    public AudioTrack currentTrack()
    {
        return player.getPlayingTrack();
    }

    public void nextTrack()
    {
        // Start the next track, regardless of if something is already playing or not. In case queue was empty, we are
        // giving null to startTrack, which is a valid argument and will simply stop the player.
        DiscordBot.discordClient.updatePresence(Presence.online()).subscribe();
        if (!sfx)
        {
            player.setVolume(MusicConfig.getInstance().getVolume());
        }
        player.startTrack(queue.poll(), false);
    }

    BlockingQueue<AudioTrack> getPlaylist()
    {
        return queue;
    }

    //TODO re-add feature
    void setLoop(boolean loop)
    {
        this.loop = loop;
    }

    @Override
    public void onTrackStart(AudioPlayer player, AudioTrack track)
    {
        if (!sfx)
        {
            if (!resume)
            {
                DiscordBot.discordClient.updatePresence(Presence.online(Activity.listening(currentTrack().getInfo().title))).subscribe();

                String playing = "Playing: " + track.getInfo().title;
                if (track.getUserData(String.class) != null)
                {
                    playing += " Requested by: " + track.getUserData(String.class);
                }

                //TODO: make this a non-fixed channel
                DiscordBot.getMessageChannel("390456848849043469").createMessage(playing).subscribe();
                for (Map.Entry<String, Long> twitchChat : Config.getInstance().getTwitchChats().entrySet())
                {
                    if (LiveStreams.checkLive(twitchChat.getKey()))
                    {
                        IRCBot.getInstance().sendMessage("#" + twitchChat.getKey(), playing);
                    }
                }
            } else
            {
                resume = false;
            }
        } else
        {
            sfx = false;
        }
    }

    @Override
    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason)
    {
        // Only start the next track if the end reason is suitable for it (FINISHED or LOAD_FAILED)
        DiscordBot.discordClient.updatePresence(Presence.online()).subscribe();
        if (endReason.mayStartNext)
        {
            if (loop & !sfx)
            {
                queue.add(track.makeClone());
            }
            nextTrack();
        } else
        {
            loop = false;
        }
    }

}