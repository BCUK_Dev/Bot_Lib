package com.danielm59.botlib.configs;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;

public class SFXfiles implements JsonData
{
    private static SFXfiles instance;
    private final transient File file = new File("sfx.json");
    @SerializedName("folder")
    private String folder = "SFX";
    @SerializedName("delay_between_SFX_in_seconds")
    private int delay = 60;
    @SerializedName("SFX_volume")
    private int volume = 100;
    @SerializedName("sounds")
    private HashMap<String, String[]> sounds = new HashMap<>();

    public SFXfiles()
    {
        this.instance = this;
    }

    public static SFXfiles getInstance()
    {
        return instance;
    }

    public String getFolder()
    {
        return folder;
    }

    public HashMap<String, String[]> getSounds()
    {
        return sounds;
    }

    public int getVolume()
    {
        return volume;
    }

    public int getDelay()
    {
        return delay;
    }

    @Override
    public File getFile()
    {
        return file;
    }
}
