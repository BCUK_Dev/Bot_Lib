package com.danielm59.botlib.configs;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MusicConfig implements JsonData
{
    private static MusicConfig instance;
    private final transient File file = new File("playlists.json");
    @SerializedName("music_volume")
    private int volume = 20;
    @SerializedName("playlists")
    private HashMap<String, String> playlists = new LinkedHashMap<>();

    public MusicConfig()
    {
        instance = this;
    }

    public static MusicConfig getInstance()
    {
        return instance;
    }

    public int getVolume()
    {
        return volume;
    }

    public void setVolume(int volume)
    {
        this.volume = volume;
        ConfigHandler.saveJson(this);
    }

    public HashMap<String, String> getPlaylists()
    {
        return playlists;
    }

    public void addPlaylist(String name, String playlist)
    {
        playlists.put(name, playlist);
        ConfigHandler.saveJson(this);
    }

    @Override
    public File getFile()
    {
        return file;
    }
}
