package com.danielm59.botlib.configs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ConfigHandler
{
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static <T extends JsonData> T loadJson(T data)
    {
        File file = data.getFile();
        if (file.exists() && !file.isDirectory())
        {
            try (FileReader reader = new FileReader(file))
            {
                JsonReader jsonReader = new JsonReader(reader);
                data = gson.fromJson(jsonReader, data.getClass());
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return saveJson(data); //Save to add anything new in the config
    }

    public static <T extends JsonData> T saveJson(T data)
    {
        File file = data.getFile();
        try (FileWriter writer = new FileWriter(file))
        {
            gson.toJson(data, writer);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return data;
    }
}
