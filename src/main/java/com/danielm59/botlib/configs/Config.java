package com.danielm59.botlib.configs;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Config implements JsonData
{
    private static Config instance;
    private final transient File file = new File("config.json");
    @SerializedName("twitch_client_id")
    private String twitchClientID = "";
    @SerializedName("twitch_botname")
    private String twitchBotname = "";
    @SerializedName("twitch_oauth")
    private String twitchOauth = "";
    @SerializedName("twitch_secret")
    private String twitchSecret = "";
    @SerializedName("discord_token")
    private String discordToken = "";
    @SerializedName("sql_server")
    private String SQLServer = "";
    @SerializedName("sql_user")
    private String SQLUser = "";
    @SerializedName("sql_password")
    private String SQLPassword = "";
    @SerializedName("discord_channel_info")
    private DiscordChannelInfo[] discordChannelInfo = {new DiscordChannelInfo()};
    @SerializedName("twitch_chats")
    private HashMap<String, Long> twitchChats = new HashMap<>();

    public Config()
    {
        instance = this;
    }

    public static Config getInstance()
    {
        return instance;
    }

    public String getTwitchClientID()
    {
        return twitchClientID;
    }

    public String getTwitchBotname()
    {
        return twitchBotname;
    }

    public String getTwitchOauth()
    {
        return twitchOauth;
    }

    public String getTwitchSecret()
    {
        return twitchSecret;
    }

    public String getDiscordToken()
    {
        return discordToken;
    }

    public Object getSQLServer()
    {
        return SQLServer;
    }

    public String getSQLUser()
    {
        return SQLUser;
    }

    public String getSQLPassword()
    {
        return SQLPassword;
    }

    public DiscordChannelInfo[] getDiscordChannelInfo()
    {
        return discordChannelInfo;
    }

    public HashMap<String, Long> getTwitchChats()
    {
        return twitchChats;
    }

    @Override
    public File getFile()
    {
        return file;
    }

    public class DiscordChannelInfo
    {
        @SerializedName("group_name")
        private String groupName = "";

        @SerializedName("channel_id")
        private String channelID = "";

        @SerializedName("delete_old_posts")
        private Boolean deleteOldPosts = false;

        @SerializedName("multi_twitch")
        private Boolean multiTwitch = false;

        @SerializedName("message_live")
        private String messageLive = "%channel% is live playing %game% - %link%";

        @SerializedName("message_new_game")
        private String messageNewGame = "%channel% is now playing %game% - %link%";

        @SerializedName("message_multi_twitch")
        private String messageMultiTwitch = "Multiple streams for %game% are live, watch them all at %link%";

        @SerializedName("streamers")
        private List<String> streamers = new ArrayList<>();

        public String getGroupName()
        {
            return groupName;
        }

        public String getChannelID()
        {
            return channelID;
        }

        public Boolean getDeleteOldPosts()
        {
            return deleteOldPosts;
        }

        public Boolean getMultiTwitch()
        {
            return multiTwitch;
        }

        public String getMessageLive()
        {
            return messageLive;
        }

        public String getMessageMultiTwitch()
        {
            return messageMultiTwitch;
        }

        public String getMessageNewGame()
        {
            return messageNewGame;
        }

        public List<String> getStreamers()
        {
            return streamers;
        }
    }
}
