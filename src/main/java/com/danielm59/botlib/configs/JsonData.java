package com.danielm59.botlib.configs;

import java.io.File;

public interface JsonData
{
    File getFile();
}
