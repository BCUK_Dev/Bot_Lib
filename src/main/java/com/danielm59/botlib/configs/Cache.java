package com.danielm59.botlib.configs;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Cache implements JsonData
{
    private static Cache instance;
    private final transient File file = new File("cache.json");
    @SerializedName("users")
    private HashMap<String, String> users = new LinkedHashMap<>();
    @SerializedName("games")
    private HashMap<String, String> games = new LinkedHashMap<>();

    public Cache()
    {
        instance = this;
    }

    public static Cache getInstance()
    {
        return instance;
    }

    public HashMap<String, String> getUsers()
    {
        return users;
    }

    public void setUsers(HashMap<String, String> users)
    {
        this.users = users;
    }

    public HashMap<String, String> getGames()
    {
        return games;
    }

    public void setGames(HashMap<String, String> games)
    {
        this.games = games;
    }

    @Override
    public File getFile()
    {
        return file;
    }
}
