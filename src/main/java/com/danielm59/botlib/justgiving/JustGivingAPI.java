package com.danielm59.botlib.justgiving;

import com.danielm59.botlib.twitch.irc.IRCBot;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JustGivingAPI
{
    private static String data;
    private static String lastTotal;

    private static BufferedReader request(URL url) throws Exception
    {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json");
        switch (conn.getResponseCode())
        {
            case 200:
                return new BufferedReader(new InputStreamReader((conn.getInputStream())));
            case 304:
                //no change
                return null;
            default:
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " From:" + url.toString());
        }
    }

    public static void updateData()
    {
        try
        {
            URL url = new URL("https://api.justgiving.com/6a2d369f/v1/fundraising/pages/battlecattlegames");
            BufferedReader br = request(url);
            String output;

            if (br != null && (output = br.readLine()) != null)
            {
                data = output;
                JsonElement jsonTree = JsonParser.parseString(data);
                if(jsonTree.isJsonObject())
                {
                    JsonObject jsonObject = jsonTree.getAsJsonObject();
                    String total = jsonObject.get("grandTotalRaisedExcludingGiftAid").getAsString();
                    if (!total.equalsIgnoreCase(lastTotal))
                    {
                        lastTotal = total;
                        sendMessageToAll();
                    }
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void sendMessageToAll()
    {
        String message = getTotalRaisedMessage();
        if(message != null)
        {
            IRCBot.getInstance().sendMessage("#mr_stephen", message);
            IRCBot.getInstance().sendMessage("#vimeous", message);
        }
    }

    public static void sendMessage(String channel)
    {
        String message = getTotalRaisedMessage();
        if(message != null)
        {
            IRCBot.getInstance().sendMessage(channel, message);
        }
    }

    public static String getTotalRaisedMessage()
    {
        JsonElement jsonTree = JsonParser.parseString(data);
        if(jsonTree.isJsonObject()) {
            JsonObject jsonObject = jsonTree.getAsJsonObject();
            String raised = jsonObject.get("grandTotalRaisedExcludingGiftAid").getAsString();
            String target = jsonObject.get("fundraisingTarget").getAsString();
            String percentage = jsonObject.get("totalRaisedPercentageOfFundraisingTarget").getAsString();
            return String.format("So far we have raised \u00A3%s out of \u00A3%s (%s%%)", raised, target, percentage);
        }
        return null;
    }

}
