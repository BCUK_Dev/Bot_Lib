package com.danielm59.botlib.sql;

import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.mojang.NameWithUUID;
import com.danielm59.botlib.mojang.Profile;
import com.danielm59.botlib.mojang.UuidApi;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLServer
{
    public static String add(String type, String data, String source)
    {
        try
        {
            String server = String.format("jdbc:mysql://%s/", Config.getInstance().getSQLServer());
            Connection con = DriverManager.getConnection(server, Config.getInstance().getSQLUser(), Config.getInstance().getSQLPassword());
            PreparedStatement preparedStmt = con.prepareStatement(String.format("insert into Quotes.%ss (%s, Source, Date) values (?, ?, ?)", type, type));
            preparedStmt.setString(1, data);
            preparedStmt.setString(2, source);
            preparedStmt.setString(3, new Date(System.currentTimeMillis()).toString());
            preparedStmt.execute();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(String.format("select * from Quotes.%ss order by ID Desc limit 1", type));
            rs.next();
            String quote = String.format("Added %s %d: %s [%s]", type, rs.getInt(1), rs.getString(2), rs.getDate(4).toString());
            con.close();
            return quote;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static String get(String type, int i)
    {
        try
        {
            String server = String.format("jdbc:mysql://%s/", Config.getInstance().getSQLServer());
            Connection con = DriverManager.getConnection(server, Config.getInstance().getSQLUser(), Config.getInstance().getSQLPassword());
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(String.format("select * from Quotes.%ss WHERE ID=%d", type, i));
            rs.next();
            String message = String.format("%s %d: %s [%s]", type, rs.getInt(1), rs.getString(2), rs.getDate(4).toString());
            con.close();
            return message;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static String getRandom(String type)
    {
        try
        {
            String server = String.format("jdbc:mysql://%s/", Config.getInstance().getSQLServer());
            Connection con = DriverManager.getConnection(server, Config.getInstance().getSQLUser(), Config.getInstance().getSQLPassword());
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(String.format("select * from Quotes.%ss ORDER BY RAND() LIMIT 1", type));
            rs.next();
            String message = String.format("%s %d: %s [%s]", type, rs.getInt(1), rs.getString(2), rs.getDate(4).toString());
            con.close();
            return message;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static String addMinecraftWhitelist(Long discordID, String username)
    {
        NameWithUUID newUser = UuidApi.nameToUUID(username);
        if(newUser != null)
        try
        {
            String server = String.format("jdbc:mysql://%s/", Config.getInstance().getSQLServer());
            Connection con = DriverManager.getConnection(server, Config.getInstance().getSQLUser(), Config.getInstance().getSQLPassword());
            PreparedStatement preparedStmt = con.prepareStatement("SELECT * FROM bcuk_mc.whitelist WHERE discordID=?");
            preparedStmt.setLong( 1, discordID);
            ResultSet rs = preparedStmt.executeQuery();
            if(rs.next())
            {
                String removed = rs.getString("mcUUID");
                Profile removedUser = UuidApi.getProfile(removed);
                preparedStmt = con.prepareStatement("UPDATE bcuk_mc.whitelist SET mcUUID=? WHERE discordID=?");
                preparedStmt.setString( 1, newUser.getId());
                preparedStmt.setLong( 2, discordID);
                preparedStmt.execute();
                con.close();
                if(removedUser != null)
                {
                    return "Replaced " + removedUser.getName() + " with " + newUser.getName() + " in the whitelist, You can only whitelist one account";
                }
                return "Replaced ?Unknown? with " + newUser.getName() + " in the whitelist, You can only whitelist one account";
            }else
            {
                preparedStmt = con.prepareStatement("INSERT INTO bcuk_mc.whitelist (discordID,mcUUID) VALUES (?,?)");
                preparedStmt.setLong( 1, discordID);
                preparedStmt.setString( 2, newUser.getId());
                preparedStmt.execute();
                con.close();
                return "Added " + newUser.getName() + " to the whitelist";
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return "Minecraft user not found";
    }
}