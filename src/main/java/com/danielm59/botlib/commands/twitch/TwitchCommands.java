package com.danielm59.botlib.commands.twitch;

import com.danielm59.botlib.commands.Counters;
import com.danielm59.botlib.commands.Function;
import com.danielm59.botlib.discord.music.MusicHandler;
import com.danielm59.botlib.jokes.JokeAPI;
import com.danielm59.botlib.justgiving.JustGivingAPI;
import com.danielm59.botlib.twitch.LiveStreams;
import com.danielm59.botlib.twitch.SFX;
import com.danielm59.botlib.twitch.irc.IRCBot;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import java.util.TreeMap;

public class TwitchCommands extends TreeMap<String, TwitchCommands.Command>
{
    public TwitchCommands()
    {
        super(String.CASE_INSENSITIVE_ORDER);
        this.put("!Joke", new Command(((c, u, args) -> SQL.processCommand("Joke", c, args))));
        this.put("!DadJoke", new Command((c, u, args) -> JokeAPI.TwitchJoke(c)));
        this.put("!Multi", new Command((c, u, args) -> LiveStreams.postMultiTwitch(c)));

        Counters.setupTwitchCommands(this);

        this.put("!Sfx", new Command((c, u, args) -> IRCBot.getInstance().sendMessage(c, SFX.getSFX())));

        this.put("!Playing", new Command((c, u, args) -> playing(c)));

        this.put("!SO", new Command((c, u, args) -> shoutOut(c, args)));

        this.put("!GameBlast", new Command(((c,u,args) -> IRCBot.getInstance().sendMessage(c,
                "We are raising money for GameBlast20, " +
                        "donate here https://www.justgiving.com/fundraising/battlecattlegames"))));
        this.put("!GameBlastTotal", new Command(((c,u,args) -> JustGivingAPI.sendMessage(c))));
    }

    private static void playing(String c)
    {
        AudioTrack track = MusicHandler.getScheduler().currentTrack();
        if (track != null)
        {
            IRCBot.getInstance().sendMessage(c, "Playing: " + track.getInfo().title);
        } else
        {
            IRCBot.getInstance().sendMessage(c, "No Track is currently Playing");
        }
    }

    private static void shoutOut(String c, String[] args)
    {
        if (args.length > 0)
        {
            String channel = args[0].replace("@", ""); //Remove at in case someone dose @Username
            IRCBot.getInstance().sendMessage(c,
                    String.format("Go and check out %s's channel over at https://www.twitch.tv/%s",
                            channel, channel));
        }
    }

    public static class Command
    {
        Function<String, String, String[]> function;

        public Command(Function<String, String, String[]> function)
        {
            this.function = function;
        }

        public Function<String, String, String[]> getFunction()
        {
            return function;
        }
    }
}
