package com.danielm59.botlib.commands.twitch;

import com.danielm59.botlib.commands.Updater;
import com.danielm59.botlib.discord.music.MusicHandler;
import org.kitteh.irc.client.library.feature.twitch.event.WhisperEvent;

import java.util.TreeMap;
import java.util.function.Consumer;

public class TwitchWhisperCommands extends TreeMap<String, TwitchWhisperCommands.Command>
{
    public TwitchWhisperCommands()
    {
        super(String.CASE_INSENSITIVE_ORDER);
        this.put("!Volume", new TwitchWhisperCommands.Command(TwitchWhisperCommands::volume));
        this.put("!Pause", new TwitchWhisperCommands.Command(TwitchWhisperCommands::pause));
        this.put("!Update", new TwitchWhisperCommands.Command(Updater::update));
    }

    public static void volume(WhisperEvent event)
    {
        String content = event.getMessage();
        String[] args = content.split(" ");
        if (args.length > 1)
        {
            MusicHandler.setVolume(event, args[1]);
        } else
        {
            MusicHandler.getVolume(event);
        }
    }

    public static void pause(WhisperEvent event)
    {
        MusicHandler.togglePause(event);
    }

    public static class Command
    {
        Consumer<WhisperEvent> function;

        public Command(Consumer<WhisperEvent> function)
        {
            this.function = function;
        }

        public Consumer<WhisperEvent> getFunction()
        {
            return function;
        }
    }
}
