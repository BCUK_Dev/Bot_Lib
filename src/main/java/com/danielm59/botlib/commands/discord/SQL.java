package com.danielm59.botlib.commands.discord;

import com.danielm59.botlib.Utils;
import com.danielm59.botlib.sql.SQLServer;
import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

class SQL
{
    private static final String SOURCE = "discord";

    static Mono<Void> processCommand(MessageCreateEvent event, String type)
    {
        String[] args = {};
        if (event.getMessage().getContent().isPresent())
            args = event.getMessage().getContent().get().split(" ");

        String msg = null;
        if (args.length == 1)
        {
            msg = SQLServer.getRandom(type);
        } else if (args[1].equalsIgnoreCase("add") && args.length > 2)
        {
            String[] quoteArray = new String[args.length - 1];
            System.arraycopy(args, 1, quoteArray, 0, quoteArray.length);
            String quote = String.join(" ", quoteArray);
            msg = SQLServer.add(type, quote, SOURCE);
        } else if (Utils.getInt(args[1]) > 0)
        {
            msg = SQLServer.get(type, Utils.getInt(args[1]));
        }
        if (msg == null)
        {
            msg = "ERROR: Incorrect syntax";
        }
        String finalMsg = msg;
        return event.getMessage().getChannel()
                .flatMap(messageChannel -> messageChannel.createMessage(finalMsg)).then();
    }
}
