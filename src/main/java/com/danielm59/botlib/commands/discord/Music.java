package com.danielm59.botlib.commands.discord;

import com.danielm59.botlib.configs.MusicConfig;
import com.danielm59.botlib.discord.music.MusicHandler;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.MessageChannel;
import discord4j.voice.VoiceConnection;
import org.apache.commons.lang3.math.NumberUtils;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;

public class Music
{
    private static VoiceConnection voiceConnection;

    public static Mono<Void> join(MessageCreateEvent event)
    {
        if (voiceConnection != null)
        {
            try
            {
                voiceConnection.disconnect();
            }finally
            {
                voiceConnection = null;
            }
        }
        Mono.justOrEmpty(event.getMember())
                .flatMap(Member::getVoiceState)
                .flatMap(VoiceState::getChannel)
                .flatMap(channel -> channel.join(spec -> spec.setProvider(MusicHandler.getProvider())))
                .subscribe(vc -> voiceConnection = vc);
        return Mono.empty();
    }

    public static Mono<Void> play(MessageCreateEvent event)
    {
        return Mono.justOrEmpty(event.getMessage().getContent())
                .map(content -> Arrays.asList(content.split(" ", 2)))
                .filter(command -> command.size() > 1)
                .doOnNext(command -> MusicHandler.loadAndPlay(event, command.get(1)))
                .then();
    }

    public static Mono<Void> playYT(MessageCreateEvent event)
    {
        return Mono.justOrEmpty(event.getMessage().getContent())
                .map(content -> Arrays.asList(content.split(" ", 2)))
                .filter(command -> command.size() > 1)
                .doOnNext(command -> MusicHandler.loadAndPlay(event, "ytsearch:" + command.get(1)))
                .then();
    }

    public static Mono<Void> skip(MessageCreateEvent event)
    {
        MusicHandler.getScheduler().nextTrack();
        return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Skipped to next track.")).then();
    }

    public static Mono<Void> stop(MessageCreateEvent event)
    {
        MusicHandler.getScheduler().clear();
        MusicHandler.getScheduler().nextTrack();
        return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Music stopped and queue cleared")).then();
    }

    public static Mono<Void> list(MessageCreateEvent event)
    {
        Optional<String> oMessage = event.getMessage().getContent();
        if (oMessage.isPresent())
        {
            String message = oMessage.get();
            String[] messageArray = message.split(" ");

            if (messageArray.length > 1 && NumberUtils.isCreatable(messageArray[1]))
            {
                return MusicHandler.listTracks(event, NumberUtils.createInteger(messageArray[1]));
            }
        }
        return MusicHandler.listTracks(event, 1);
    }

    public static Mono<Void> playing(MessageCreateEvent event)
    {

        AudioTrack track = MusicHandler.getScheduler().currentTrack();
        if (track != null)
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Playing: " + track.getInfo().title)).then();
        } else
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("No Track is currently Playing")).then();
        }
    }

    public static Mono<Void> playlist(MessageCreateEvent event)
    {
        Optional<String> oMessage = event.getMessage().getContent();
        if (oMessage.isPresent())
        {
            String message = oMessage.get();
            String[] messageArray = message.split(" ");

            if (messageArray.length > 1)
            {
                HashMap<String, String> playlists = MusicConfig.getInstance().getPlaylists();
                if (playlists.containsKey(messageArray[1].toLowerCase()))
                {
                    MusicHandler.loadAndPlay(event, playlists.get(messageArray[1].toLowerCase()));
                    return Mono.empty();
                } else
                {
                    return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Playlist not found")).then();
                }
            }
        }
        return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Playlist not given")).then();
    }

    public static Mono<Void> playlists(MessageCreateEvent event)
    {
        Set<String> playlists = MusicConfig.getInstance().getPlaylists().keySet();
        if (playlists.size() > 0)
        {
            StringBuilder s = new StringBuilder();
            for (String playlist : playlists)
            {
                s.append(playlist);
                s.append("\n");
            }
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage(s.toString())).then();
        } else
        {
            return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("No playlists founds")).then();
        }
    }

    public static Mono<Void> addPlaylist(MessageCreateEvent event)
    {
        Optional<String> content = event.getMessage().getContent();
        if (content.isPresent())
        {
            String[] args = content.get().split(" ");
            if (args.length > 2)
            {
                MusicConfig.getInstance().addPlaylist(args[1].toLowerCase(), args[2]);
                return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Added playlist: " + args[1].toLowerCase())).then();
            }
        }
        return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Expected name and playlist link/ID")).then();
    }

    public static Mono<Void> volume(MessageCreateEvent event)
    {
        Optional<String> content = event.getMessage().getContent();
        if (content.isPresent())
        {
            String[] args = content.get().split(" ");
            if (args.length > 1)
            {
                return MusicHandler.setVolume(event, args[1]);
            } else
            {
                return MusicHandler.getVolume(event);
            }
        }
        return Mono.empty();
    }

    public static Mono<Void> pause(MessageCreateEvent event)
    {
        return MusicHandler.togglePause(event);
    }

    public static Mono<Void> leave(MessageCreateEvent event)
    {
        Mono<MessageChannel> channel = event.getMessage().getChannel();
        if(voiceConnection != null)
        {
            try
            {
                MusicHandler.getScheduler().clear();
                MusicHandler.getScheduler().nextTrack();
                voiceConnection.disconnect();
            }finally
            {
                voiceConnection = null;
            }
            return channel.flatMap(mc -> mc.createMessage("Music stopped and queue cleared"))
                    .then(channel.flatMap(mc -> mc.createMessage("Goodbye")).then());
        }
        return channel.flatMap(mc -> mc.createMessage("I can't leave a room if i'm not in one")).then();
    }
}
