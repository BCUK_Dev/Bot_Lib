package com.danielm59.botlib.commands.discord;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.Snowflake;

public class Permissions
{
    public static boolean admin(MessageCreateEvent event)
    {
        return event.getMember().map(m -> m.getBasePermissions().map(ps -> ps.contains(Permission.MANAGE_MESSAGES)).block()).orElse(false);
    }

    public static boolean general(MessageCreateEvent event)
    {
        return true;
    }

    public static boolean hasRole(MessageCreateEvent event, Snowflake roleID)
    {
        return event.getMember().map(value -> value.getRoleIds().contains(roleID)).orElse(false);
    }
}
