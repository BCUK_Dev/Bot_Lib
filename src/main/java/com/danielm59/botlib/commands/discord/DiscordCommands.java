package com.danielm59.botlib.commands.discord;

import com.danielm59.botlib.BuildInfo;
import com.danielm59.botlib.commands.Updater;
import com.danielm59.botlib.discord.pointSystem.PointSystem;
import com.danielm59.botlib.jokes.JokeAPI;
import com.danielm59.botlib.twitch.SFX;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.Role;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.TreeMap;

public class DiscordCommands
{
    private TreeMap<CommandCategory, Map<String, Command>> commands = new TreeMap<>();

    public DiscordCommands()
    {
        //TODO only load commands for modules being used

        for (CommandCategory category : CommandCategory.values())
        {
            commands.put(category, new TreeMap<>(String.CASE_INSENSITIVE_ORDER));
        }

        //General
        commands.get(CommandCategory.GENERAL).put("!Commands", new Command(this::commands, Permissions::general));

        commands.get(CommandCategory.ADMIN).put("!Roles", new Command(this::roles, Permissions::admin));
        commands.get(CommandCategory.ADMIN).put("!Update", new Command(Updater::update, Permissions::admin));
        commands.get(CommandCategory.ADMIN).put("!Version", new Command(this::version, Permissions::admin));

        //SQL
        commands.get(CommandCategory.GENERAL).put("!Quote", new Command(event -> SQL.processCommand(event, "Quote"), Permissions::general));
        commands.get(CommandCategory.GENERAL).put("!Clip", new Command(event -> SQL.processCommand(event, "Clip"), Permissions::general));
        commands.get(CommandCategory.GENERAL).put("!Joke", new Command(event -> SQL.processCommand(event, "Joke"), Permissions::general));

        //Levels
        commands.get(CommandCategory.GENERAL).put("!Points", new Command(PointSystem::points, Permissions::general));

        //music
        commands.get(CommandCategory.MUSIC).put("!Join", new Command(Music::join, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Play", new Command(Music::play, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!YT", new Command(Music::playYT, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Skip", new Command(Music::skip, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Stop", new Command(Music::stop, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!List", new Command(Music::list, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Playing", new Command(Music::playing, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Playlist", new Command(Music::playlist, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Playlists", new Command(Music::playlists, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!AddPlaylist", new Command(Music::addPlaylist, Permissions::admin));
        commands.get(CommandCategory.MUSIC).put("!Volume", new Command(Music::volume, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Pause", new Command(Music::pause, Permissions::general));
        commands.get(CommandCategory.MUSIC).put("!Leave", new Command(Music::leave, Permissions::general));

        commands.get(CommandCategory.GENERAL).put("!DadJoke", new Command(JokeAPI::DiscordJoke, Permissions::general));

        commands.get(CommandCategory.GENERAL).put("!SFX", new Command(this::sfx, Permissions::general));

        commands.get(CommandCategory.MINECRAFT).put("!Whitelist", new Command(Minecraft::whitelist, event -> Permissions.hasRole(event, Snowflake.of(489887389725229066L))));
    }

    public Mono<Void> processCommand(MessageCreateEvent event)
    {
        Message message = event.getMessage();

        String[] command = {""};
        if (message.getContent().isPresent())
            command = message.getContent().get().split(" ", 2);

        for (Map<String, Command> category : commands.values())
        {
            if (category.containsKey(command[0].toLowerCase()))
            {
                String[] args = {};
                if (command.length > 1)
                    args = command[1].split(" ");
                Command com = category.get(command[0].toLowerCase());

                if (com.getPermission().apply(event))
                {
                    return com.runTask(event).onErrorContinue((t,o) -> Mono.empty().then());
                } else
                {
                    return message.getChannel().flatMap(c -> c.createMessage("You can't do that!")).then();
                }
            }
        }
        return Mono.empty();
    }

    public void registerCommand(CommandCategory category, String commandName, Command command)
    {
        commands.get(category).put(commandName, command);
    }

    private Mono<Void> commands(MessageCreateEvent event)
    {
        StringBuilder s = new StringBuilder();
        s.append("```\nAvailable commands:\n");
        for (CommandCategory category : commands.keySet())
        {
            s.append(String.format("\n%s\n", category.toString()));
            s.append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

            for (String key : commands.get(category).keySet())
            {
                Command command = commands.get(category).get(key);
                if (command.getPermission().apply(event))
                    s.append(String.format("%s\n", key));
            }
        }
        s.append("```");
        return event.getMessage().getChannel().flatMap(channel -> channel.createMessage(s.toString())).then();
    }

    private Mono<Void> sfx(MessageCreateEvent event)
    {
        return event.getMessage().getChannel().flatMap(
                channel -> channel.createMessage(SFX.getSFX())).then();
    }

    private Mono<Void> roles(MessageCreateEvent event)
    {
        StringBuilder s = new StringBuilder();
        s.append("```\nRoles:\n");

        Guild guild = event.getGuild().block();
        if (guild != null)
        {
            for (Role role : guild.getRoles().toIterable())
            {
                s.append(role.getName());
                s.append(":");
                s.append(role.getId().asString());
                s.append("\n");
            }
            s.append("```");
        } else
        {
            s.append("error getting guild from message");
        }
        return event.getMember()
                .map(User::getPrivateChannel).get()
                .flatMap(privateChannel -> privateChannel.createMessage(s.toString()))
                .then();
    }

    private Mono<Void> version(MessageCreateEvent event)
    {
        return event.getMessage().getChannel()
                .flatMap(messageChannel -> messageChannel.createMessage(BuildInfo.getVersion()))
                .then();
    }

}
