package com.danielm59.botlib.commands.discord;

import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

import java.util.function.Function;

public class Command
{
    CommandTask task;
    Function<MessageCreateEvent, Boolean> permission;

    public Command(CommandTask task, Function<MessageCreateEvent, Boolean> permission)
    {
        this.task = task;
        this.permission = permission;
    }

    public Function<MessageCreateEvent, Boolean> getPermission()
    {
        return permission;
    }

    public Mono<Void> runTask(MessageCreateEvent event)
    {
        return task.execute(event);
    }
}
