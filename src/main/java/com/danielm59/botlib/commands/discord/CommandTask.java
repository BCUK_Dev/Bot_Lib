package com.danielm59.botlib.commands.discord;

import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

public interface CommandTask
{
    Mono<Void> execute(MessageCreateEvent event);
}
