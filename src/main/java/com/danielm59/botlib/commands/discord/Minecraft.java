package com.danielm59.botlib.commands.discord;

import com.danielm59.botlib.sql.SQLServer;
import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class Minecraft
{
    public static Mono<Void> whitelist(MessageCreateEvent event)
    {
        AtomicReference<String> message = new AtomicReference<>("");
        Optional<String> content = event.getMessage().getContent();
        if(content.isPresent())
        {
            String[] splitMessage = content.get().split(" ");
            if (splitMessage.length < 2)
                return event.getMessage().getChannel().flatMap(mc -> mc.createMessage("Username not given")).then();
            event.getMember().ifPresent(member -> message.set(SQLServer.addMinecraftWhitelist(member.getId().asLong(), splitMessage[1])));
            if (!message.get().equals(""))
                return event.getMessage().getChannel().flatMap(mc -> mc.createMessage(message.get())).then();
        }
        return Mono.empty();
    }
}
