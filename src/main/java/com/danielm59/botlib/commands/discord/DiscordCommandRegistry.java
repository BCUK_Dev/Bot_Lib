package com.danielm59.botlib.commands.discord;

public interface DiscordCommandRegistry
{
    void registerCommands(DiscordCommands commands);
}
