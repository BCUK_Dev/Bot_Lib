package com.danielm59.botlib.commands.discord;

public enum CommandCategory
{
    GENERAL, ADMIN, MUSIC, COUNTERS,COUNTERS_CHECK, GAMES, MINECRAFT
}
