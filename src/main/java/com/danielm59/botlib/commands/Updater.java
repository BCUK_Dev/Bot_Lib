package com.danielm59.botlib.commands;

import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.io.IOException;

public class Updater
{
    private static final Logger logger = LoggerFactory.getLogger(Updater.class);

    public static Mono<Void> update(Object ignored)
    {
        try
        {
            if (SystemUtils.IS_OS_WINDOWS)
            {
                Runtime.getRuntime().exec(new String[]{"cmd.exe", "/c", "start java -jar Updater.jar"});
            } else if (SystemUtils.IS_OS_LINUX)
            {
                Runtime.getRuntime().exec(new String[]{"xfce4-terminal", "-e", "java -jar Updater.jar"});
            }
            System.exit(0);
        } catch (IOException e)
        {
            logger.error(e.toString());
        }
        return Mono.empty().then();
    }
}
