package com.danielm59.botlib.commands;

import com.danielm59.botlib.commands.discord.Command;
import com.danielm59.botlib.commands.discord.CommandCategory;
import com.danielm59.botlib.commands.discord.DiscordCommandRegistry;
import com.danielm59.botlib.commands.discord.DiscordCommands;
import com.danielm59.botlib.commands.discord.Permissions;
import com.danielm59.botlib.commands.twitch.TwitchCommands;
import com.danielm59.botlib.configs.ConfigHandler;
import com.danielm59.botlib.configs.JsonData;
import com.danielm59.botlib.twitch.irc.IRCBot;
import discord4j.core.event.domain.message.MessageCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.io.File;
import java.util.HashMap;

public class Counters implements JsonData, DiscordCommandRegistry
{
    private static Counters instance;
    private final transient File file = new File("counters.json");
    private final transient Logger logger = LoggerFactory.getLogger(Counters.class);
    private HashMap<String, Counter> counters = new HashMap<>();

    public Counters()
    {
        instance = this;
    }

    public static Counters getInstance()
    {
        return instance;
    }

    public static Mono<Void> counter(MessageCreateEvent event, String name)
    {
        return event.getMessage().getChannel().flatMap(messageChannel -> messageChannel.createMessage(instance.increment(name))).then();
    }

    public static Mono<Void> counterCheck(MessageCreateEvent event, String name)
    {
        return event.getMessage().getChannel().flatMap(messageChannel -> messageChannel.createMessage(instance.get(name))).then();
    }

    private static void counter(String c, String name)
    {
        IRCBot.getInstance().sendMessage(c, Counters.getInstance().increment(name));
    }

    private static void counterCheck(String c, String name)
    {
        IRCBot.getInstance().sendMessage(c, Counters.getInstance().get(name));
    }

    public void registerCommands(DiscordCommands commands)
    {
        for (String counter : instance.counters.keySet())
        {
            commands.registerCommand(CommandCategory.COUNTERS, "#" + counter, new Command(e -> counter(e, counter), Permissions::general));
            commands.registerCommand(CommandCategory.COUNTERS_CHECK, "!" + counter + "Check", new Command(event -> Counters.counterCheck(event, counter), Permissions::general));
        }
    }

    public static void setupTwitchCommands(TwitchCommands commands)
    {
        for (String counter : instance.counters.keySet())
        {
            commands.put("#" + counter, new TwitchCommands.Command((c, u, args) -> counter(c, counter)));
            commands.put("!" + counter + "Check", new TwitchCommands.Command((c, u, args) -> Counters.counterCheck(c, counter)));
        }
    }

    @Override
    public File getFile()
    {
        return file;
    }

    public String increment(String name)
    {
        if (counters.containsKey(name))
        {
            String message = counters.get(name).increment();
            ConfigHandler.saveJson(this);
            return message;
        } else
        {
            logger.error(String.format("Counter %s not found", name));
            return null;
        }
    }

    public String get(String name)
    {
        if (counters.containsKey(name))
        {
            String message = counters.get(name).get();
            ConfigHandler.saveJson(this);
            return message;
        } else
        {
            logger.error(String.format("Counter %s not found", name));
            return null;
        }
    }

    static class Counter
    {
        String incrementMessage;
        String message;
        int count;

        String increment()
        {
            return String.format(incrementMessage + "     " + message, ++count);
        }

        String get()
        {
            return String.format(message, count);
        }
    }
}
