package com.danielm59.botlib.twitch.irc;

import com.danielm59.botlib.commands.twitch.TwitchCommands;
import com.danielm59.botlib.commands.twitch.TwitchWhisperCommands;
import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.twitch.SFX;
import net.engio.mbassy.listener.Handler;
import org.kitteh.irc.client.library.element.MessageTag;
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent;
import org.kitteh.irc.client.library.event.client.ClientNegotiationCompleteEvent;
import org.kitteh.irc.client.library.feature.twitch.event.UserNoticeEvent;
import org.kitteh.irc.client.library.feature.twitch.event.WhisperEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class IRCListener
{
    private static final Logger logger = LoggerFactory.getLogger(IRCListener.class);
    public static TwitchCommands commands = new TwitchCommands();
    public static TwitchWhisperCommands whisperCommands = new TwitchWhisperCommands();

    @Handler
    protected void onConnect(ClientNegotiationCompleteEvent event)
    {
        logger.info("Connected to Twitch chat server");
        for (String channel : Config.getInstance().getTwitchChats().keySet())
        {
            IRCBot.join(channel);
        }
    }

    @Handler
    protected void onMessage(ChannelMessageEvent event)
    {
        String[] command = event.getMessage().split(" ", 2);

        if (commands.containsKey(command[0].toLowerCase()))
        {
            String[] args = {};
            if (command.length > 1)
                args = command[1].split(" ");
            commands.get(command[0].toLowerCase()).getFunction().accept(event.getChannel().getName(), event.getActor().getNick(), args);
        }
        SFX.play(command[0]);
    }

    @Handler
    protected void onWhisper(WhisperEvent event)
    {
        String[] command = event.getMessage().split(" ", 2);

        if (whisperCommands.containsKey(command[0].toLowerCase()))
        {
            whisperCommands.get(command[0].toLowerCase()).getFunction().accept(event);
        }
    }

    @Handler
    protected void onUserNoticeEvent(UserNoticeEvent event)
    {
        List<MessageTag> tags = event.getSource().getTags();
        if (tags.size() > 0)
        {
            File file = new File("TwitchUserNoticeEvents.txt");
            try
            {
                file.createNewFile();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
            try (FileWriter writer = new FileWriter(file, true))
            {
                StringBuilder s = new StringBuilder();
                for (MessageTag tag : tags)
                {
                    s.append(tag.getAsString()).append(" ");
                }
                s.append("\n");
                writer.write(s.toString());
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

}
