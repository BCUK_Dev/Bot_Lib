package com.danielm59.botlib.twitch.irc;

import com.danielm59.botlib.configs.Config;
import org.kitteh.irc.client.library.Client;
import org.kitteh.irc.client.library.feature.twitch.TwitchSupport;

public class IRCBot
{
    private static Client ircClient;

    public IRCBot()
    {
        ircClient = Client.builder()
                .server().host("irc.chat.twitch.tv").port(443)
                .password(Config.getInstance().getTwitchOauth()).then()
                .nick(Config.getInstance().getTwitchBotname())
                .build();
        TwitchSupport.addSupport(ircClient);
        ircClient.getEventManager().registerEventListener(new IRCListener());
        ircClient.connect();
    }

    public static Client getInstance()
    {
        return ircClient;
    }

    public static void join(String channel)
    {
        ircClient.addChannel("#" + channel);
    }

}