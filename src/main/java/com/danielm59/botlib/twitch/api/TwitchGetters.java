package com.danielm59.botlib.twitch.api;

import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.twitch.api.data.GameData;
import com.danielm59.botlib.twitch.api.data.StreamData;
import com.danielm59.botlib.twitch.api.data.UserData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

public class TwitchGetters
{
    private static String accessToken = "";

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private static void checkAccessToken() throws Exception
    {
        HttpURLConnection conn = (HttpURLConnection) (new URL("https://id.twitch.tv/oauth2/validate")).openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", "OAuth " + accessToken);
        int response = conn.getResponseCode();
        if (response == 200)
        {
            JsonElement json = JsonParser.parseReader(new InputStreamReader(conn.getInputStream()));
            if (json.isJsonObject())
            {
                if (((JsonObject) json).get("expires_in").getAsInt() < 3600)
                    renewAccessToken();
            }
            return;
        }
        if (response == 401)
        {
            renewAccessToken();
            return;
        }
        throw new RuntimeException("Failed : HTTP error code : " + response + " checking access token");
    }

    private static void renewAccessToken() throws Exception
    {
        String url = "https://id.twitch.tv/oauth2/token?" +
                "client_id="+ URLEncoder.encode(Config.getInstance().getTwitchClientID(), "UTF-8") +
                "&client_secret=" + URLEncoder.encode(Config.getInstance().getTwitchSecret(), "UTF-8")+
                "&grant_type=client_credentials";

        HttpURLConnection conn = (HttpURLConnection) (new URL(url)).openConnection();
        conn.setRequestMethod("POST");
        conn.connect();

        if (conn.getResponseCode() != 200)
        {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " Getting access token");
        }
        JsonElement json = JsonParser.parseReader(new InputStreamReader(conn.getInputStream()));
        if (json.isJsonObject())
        {
            accessToken = ((JsonObject) json).get("access_token").getAsString();
        } else
            throw new RuntimeException(("Error reading access token"));
    }

    private static BufferedReader request(URL url) throws Exception
    {
        checkAccessToken();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + accessToken);
        conn.setRequestProperty("Client-ID", Config.getInstance().getTwitchClientID());
        int response = conn.getResponseCode();
        if (response != 200)
        {
            if(response == 401)
            {
                String message = "Unknown 401 error";
                JsonElement json = JsonParser.parseReader(new InputStreamReader(conn.getErrorStream()));
                if (json.isJsonObject())
                {
                    message = ((JsonObject) json).get("message").getAsString();
                }
                throw new RuntimeException("401: " + message);
            }
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " From:" + url.toString());
        }
        return new BufferedReader(new InputStreamReader((conn.getInputStream())));
    }

    public static StreamData getStreamData(List<String> channels) throws Exception
    {
        String request = String.join("&user_login=", channels);
        URL url = new URL(String.format("https://api.twitch.tv/helix/streams?user_login=%s", request));
        BufferedReader br = request(url);

        String output;
        if ((output = br.readLine()) != null)
        {
            return gson.fromJson(output, StreamData.class);
        }
        return null;
    }

    public static GameData getGameData(String gameID) throws Exception
    {
        URL url = new URL(String.format("https://api.twitch.tv/helix/games?id=%s", gameID));
        BufferedReader br = request(url);

        String output;
        if ((output = br.readLine()) != null)
        {
            return gson.fromJson(output, GameData.class);
        }
        return null;
    }

    public static UserData getUserData(String userID) throws Exception
    {
        URL url = new URL(String.format("https://api.twitch.tv/helix/users?id=%s", userID));
        BufferedReader br = request(url);

        String output;
        if ((output = br.readLine()) != null)
        {
            return gson.fromJson(output, UserData.class);
        }
        return null;
    }
}