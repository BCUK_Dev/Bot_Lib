package com.danielm59.botlib.twitch.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GameData
{

    @SerializedName("data")
    private List<Datum> data = null;

    public Datum getData()
    {
        return (data.size() > 0) ? data.get(0) : null;
    }

    public class Datum
    {
        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("box_art_url")
        private String boxArtUrl;

        public String getId()
        {
            return id;
        }

        public String getName()
        {
            return name;
        }

        public String getBoxArtUrl()
        {
            return boxArtUrl;
        }
    }
}
