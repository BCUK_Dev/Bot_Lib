package com.danielm59.botlib.twitch.api;

import com.danielm59.botlib.configs.Cache;
import com.danielm59.botlib.configs.ConfigHandler;
import com.danielm59.botlib.twitch.api.data.GameData;
import com.danielm59.botlib.twitch.api.data.UserData;

public class CacheGetters
{
    public static String getUsername(String userID) throws Exception
    {
        if (Cache.getInstance().getUsers().containsKey(userID))
        {
            return Cache.getInstance().getUsers().get(userID);
        } else
        {
            UserData data = TwitchGetters.getUserData(userID);
            String username = data.getData().getDisplayName();
            Cache.getInstance().getUsers().put(userID, username);
            ConfigHandler.saveJson(Cache.getInstance());
            return username;
        }
    }

    public static String getGame(String gameID) throws Exception
    {
        String gamename;
        if (Cache.getInstance().getGames().containsKey(gameID))
        {
            gamename = Cache.getInstance().getGames().get(gameID);
        } else
        {
            GameData data = TwitchGetters.getGameData(gameID);
            gamename = data.getData().getName();
            Cache.getInstance().getGames().put(gameID, gamename);
            ConfigHandler.saveJson(Cache.getInstance());
        }
        return gamename;
    }
}
