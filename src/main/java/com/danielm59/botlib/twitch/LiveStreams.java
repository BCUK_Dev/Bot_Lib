package com.danielm59.botlib.twitch;

import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.discord.DiscordBot;
import com.danielm59.botlib.twitch.api.CacheGetters;
import com.danielm59.botlib.twitch.api.TwitchGetters;
import com.danielm59.botlib.twitch.api.data.StreamData;
import com.danielm59.botlib.twitch.irc.IRCBot;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LiveStreams
{

    private static final Logger logger = LoggerFactory.getLogger(LiveStreams.class);
    private static HashMap<String, HashMap<String, LiveStream>> groups = new HashMap<>();
    private static HashMap<String, MultiTwitch> multiTwitchs = new HashMap<>();

    static void processStreams(Config.DiscordChannelInfo info)
    {
        try
        {
            String group = info.getGroupName();

            if (!groups.containsKey(group))
            {
                groups.put(group, new HashMap<>());
            }
            HashMap<String, LiveStream> groupData = groups.get(group);

            processLiveStreams(info, groupData);
            removeOldStreams(info, groupData);
            if (info.getMultiTwitch()) processMultiTwitch(info, groupData);

        } catch (RuntimeException e)
        {
            logger.error(e.getMessage());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void processLiveStreams(Config.DiscordChannelInfo info, HashMap<String, LiveStream> groupData) throws Exception
    {
        MessageChannel channel = DiscordBot.getMessageChannel(info.getChannelID());
        List<StreamData.Stream> streams = TwitchGetters.getStreamData(info.getStreamers()).getData();
        for (StreamData.Stream stream : streams)
        {
            String user = CacheGetters.getUsername(stream.getUserId());
            String game = (stream.getGameId().equals("") || stream.getGameId().equals("0")) ? "?Unknown Game?" : CacheGetters.getGame(stream.getGameId());
            if (groupData.containsKey(user.toLowerCase()))
            {
                LiveStream liveStream = groupData.get(user.toLowerCase());
                if (!game.equals(liveStream.game))
                {
                    if (info.getDeleteOldPosts())
                    {
                        DiscordBot.getMessage(liveStream.discordMessage.getChannelId(), liveStream.discordMessage.getId())
                                .onErrorStop().flatMap(Message::delete).subscribe();
                    }
                    liveStream.discordMessage = formatAndSend(channel, info.getMessageNewGame(), user, game);
                    liveStream.game = game;

                }
                liveStream.lastOnline = System.currentTimeMillis();
            } else
            {
                Message message = formatAndSend(channel, info.getMessageLive(), user, game);
                groupData.put(user.toLowerCase(), new LiveStream(message, game, System.currentTimeMillis()));
            }
        }
    }


    private static void removeOldStreams(Config.DiscordChannelInfo info, HashMap<String, LiveStream> groupData)
    {
        Iterator<Map.Entry<String, LiveStream>> it = groupData.entrySet().iterator();
        while (it.hasNext())
        {
            String user = it.next().getKey();
            LiveStream liveStream = groupData.get(user);

            if (System.currentTimeMillis() - liveStream.lastOnline > TimeUnit.MINUTES.toMillis(5))
            {
                if (info.getDeleteOldPosts())
                {
                    DiscordBot.getMessage(liveStream.discordMessage.getChannelId(), liveStream.discordMessage.getId())
                            .onErrorStop().flatMap(Message::delete).subscribe();
                }
                it.remove();
            }
        }
    }

    private static void processMultiTwitch(Config.DiscordChannelInfo info, HashMap<String, LiveStream> groupData)
    {

        String group = info.getGroupName();
        MessageChannel channel = DiscordBot.getMessageChannel(info.getChannelID());
        HashMap<String, List<String>> currentStreams = new HashMap<>();
        for (String user : groupData.keySet())
        {
            LiveStream liveStream = groupData.get(user);
            if (!currentStreams.containsKey(liveStream.game))
            {
                currentStreams.put(liveStream.game, new ArrayList<>());
            }
            currentStreams.get(liveStream.game).add(user);
        }
        for (String game : currentStreams.keySet())
        {
            String id = String.format("%s:%s", group, game);
            if (multiTwitchs.containsKey(id))
            {
                MultiTwitch multiTwitch = multiTwitchs.get(id);
                if (!currentStreams.get(game).containsAll(multiTwitch.users) || !multiTwitch.users.containsAll(currentStreams.get(game)))
                {
                    if (info.getDeleteOldPosts())
                    {
                        DiscordBot.getMessage(multiTwitch.message.getChannelId(), multiTwitch.message.getId())
                                .onErrorStop().flatMap(Message::delete).subscribe();
                    }
                    if (currentStreams.get(game).size() > 1)
                    {
                        multiTwitch.message = formatAndSendMulti(channel, info.getMessageMultiTwitch(), currentStreams.get(game), game);
                        multiTwitch.users = currentStreams.get(game);

                    } else
                    {
                        multiTwitchs.remove(id);
                    }
                }
                multiTwitch.lastOnline = System.currentTimeMillis();
            } else
            {
                if (currentStreams.get(game).size() > 1)
                {
                    Message message = formatAndSendMulti(channel, info.getMessageMultiTwitch(), currentStreams.get(game), game);
                    multiTwitchs.put(id, new MultiTwitch(currentStreams.get(game), message, System.currentTimeMillis()));
                }
            }
        }

        for (String id : multiTwitchs.keySet())
        {
            MultiTwitch multiTwitch = multiTwitchs.get(id);

            if (System.currentTimeMillis() - multiTwitch.lastOnline > TimeUnit.SECONDS.toMillis(30))
            {
                if (info.getDeleteOldPosts())
                {
                    DiscordBot.getMessage(multiTwitch.message.getChannelId(), multiTwitch.message.getId())
                            .onErrorStop().flatMap(Message::delete).subscribe();
                }
                multiTwitchs.remove(id);
            }
        }
    }

    private static Message formatAndSend(MessageChannel channel, String message, String user, String game)
    {
        message = message.replace("%channel%", user);
        message = message.replace("%game%", game);
        message = message.replace("%link%", String.format("https://www.twitch.tv/%s", user.toLowerCase()));
        String finalMessage = message;
        return channel.createMessage(finalMessage).block();
    }

    private static Message formatAndSendMulti(MessageChannel channel, String message, List<String> users, String game)
    {
        message = message.replace("%game%", game);
        message = message.replace("%link%", createLink(users));
        String finalMessage = message;
        return channel.createMessage(finalMessage).block();
    }

    public static void postMultiTwitch(String c)
    {
        String link = getMultiLink(c.replace("#", ""));
        if (link != null)
            IRCBot.getInstance().sendMessage(c, link);

    }

    private static String getMultiLink(String c)
    {
        for (Map.Entry<String, MultiTwitch> multiTwitch : multiTwitchs.entrySet())
        {
            if (multiTwitch.getValue().users.stream().anyMatch(s -> s.equalsIgnoreCase(c)))
            {
                return createLink(multiTwitch.getValue().users);
            }
        }
        return null;
    }

    private static String createLink(List<String> users)
    {
        StringBuilder link = new StringBuilder();
        link.append("http://multitwitch.tv/");
        for (String user : users)
        {
            link.append(user);
            link.append("/");
        }
        return link.toString();
    }

    public static boolean checkLive(String channel)
    {
        for (HashMap<String, LiveStream> group : groups.values())
        {
            if (group.containsKey(channel.toLowerCase())) return true;
        }
        return false;
    }


    static class LiveStream
    {
        private Message discordMessage;
        private String game;
        private Long lastOnline;

        LiveStream(Message discordMessage, String game, Long lastOnline)
        {
            this.discordMessage = discordMessage;
            this.game = game;
            this.lastOnline = lastOnline;
        }
    }

    static class MultiTwitch
    {
        List<String> users;
        Message message;
        Long lastOnline;

        MultiTwitch(List<String> users, Message message, Long lastOnline)
        {
            this.users = users;
            this.message = message;
            this.lastOnline = lastOnline;
        }
    }
}
