package com.danielm59.botlib.twitch;

import com.danielm59.botlib.configs.SFXfiles;
import com.danielm59.botlib.discord.music.MusicHandler;

import java.io.File;
import java.util.Random;

public class SFX
{

    private static Random rng = new Random();
    private static long lastSFX = -1;

    public static void play(String message)
    {
        long time = System.currentTimeMillis();
        if ((time - lastSFX > SFXfiles.getInstance().getDelay() * 1000) &
                SFXfiles.getInstance().getSounds().containsKey(message.toLowerCase()))
        {
            String sound = pickSound(SFXfiles.getInstance().getSounds().get(message.toLowerCase()));
            MusicHandler.loadAndPlayPriority(getFilePath(sound));
            lastSFX = time;
        }
    }

    private static String pickSound(String[] sounds)
    {
        return sounds[rng.nextInt(sounds.length)];

    }

    private static String getFilePath(String sound)
    {
        return "." +
                File.separator +
                SFXfiles.getInstance().getFolder() +
                File.separator +
                sound;
    }

    public static String getSFX()
    {
        StringBuilder SFXlist = new StringBuilder();
        for (String sfx : SFXfiles.getInstance().getSounds().keySet())
        {
            SFXlist.append(sfx).append(", ");
        }
        SFXlist.setLength(Math.max(SFXlist.length() - 2, 0));//remove last comma
        return SFXlist.toString();
    }
}
