package com.danielm59.botlib.twitch;

import com.danielm59.botlib.configs.Config;

public class OnlineTracker
{
    public static void checkStatus()
    {
        try
        {
            for (Config.DiscordChannelInfo info : Config.getInstance().getDiscordChannelInfo())
            {
                LiveStreams.processStreams(info);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
