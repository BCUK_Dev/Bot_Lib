package com.danielm59.botlib.games.slot;

import com.danielm59.botlib.commands.discord.Command;
import com.danielm59.botlib.commands.discord.CommandCategory;
import com.danielm59.botlib.commands.discord.DiscordCommandRegistry;
import com.danielm59.botlib.commands.discord.DiscordCommands;
import com.danielm59.botlib.commands.discord.Permissions;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.spec.EmbedCreateSpec;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class SlotGame implements DiscordCommandRegistry
{
    private static final Random rng = new Random();
    private static final String[] emojis = {"\uD83C\uDF47", "\uD83C\uDF4A", "\uD83C\uDF52", "\uD83C\uDF53"};
    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private static final long delay = 1L;

    public Mono<Void> startGame(MessageCreateEvent event)
    {
        Outcome outcome = getOutcome();
        String[] display = new String[outcome.result.length];
        Arrays.fill(display, "\u2753");
        Consumer<EmbedCreateSpec> embed = spec -> spec.setTitle("Slot");
        Consumer<EmbedCreateSpec> embedWithResult = embed.andThen(spec ->
                spec.addField("Outcome", String.join(" ", display), false)
        );
        Message message = event.getMessage().getChannel()
                .flatMap(channel -> channel.createMessage(messageSpec -> messageSpec.setEmbed(embedWithResult))).block();
        scheduler.schedule(() -> update(message, embed, outcome, display, 0), delay, TimeUnit.SECONDS);
        return Mono.empty().then();
    }

    private void update(Message message, Consumer<EmbedCreateSpec> embed, Outcome outcome, String[] display, int i)
    {
        display[i] = outcome.result[i];
        Consumer<EmbedCreateSpec> embedWithResult = embed.andThen(spec ->
                spec.addField("Outcome", String.join(" ", display), false)
        );

        message.edit(messageEditSpec -> messageEditSpec.setEmbed(embedWithResult)).block();
        if (++i < outcome.result.length)
        {
            int newIndex = i;
            scheduler.schedule(() -> update(message, embed, outcome, display, newIndex), delay, TimeUnit.SECONDS);
        } else
        {
            scheduler.schedule(() -> showResult(message, embedWithResult, outcome, display), delay, TimeUnit.SECONDS);

        }
    }

    private void showResult(Message message, Consumer<EmbedCreateSpec> embed, Outcome outcome, String[] display)
    {
        Consumer<EmbedCreateSpec> embedWithResult = embed.andThen(spec ->
                spec.addField("Result", (outcome.win ? "\u2705" : "\u274c"), false)
        );
        message.edit(messageEditSpec -> messageEditSpec.setEmbed(embedWithResult)).block();
    }

    @Override
    public void registerCommands(DiscordCommands commands)
    {
        commands.registerCommand(CommandCategory.GAMES, "!Slot", new Command(this::startGame, Permissions::general));
    }


    public Outcome getOutcome()
    {
        return new Outcome();
    }

    public static class Outcome
    {
        public String[] result;
        public boolean win;

        Outcome()
        {
            result = new String[]{random(), random(), random()};
            win = result[0].equals(result[1]) && result[1].equals(result[2]);
        }

        private String random()
        {
            return emojis[rng.nextInt(emojis.length)];
        }
    }
}
