package com.danielm59.botlib.games.trivia;

import com.danielm59.botlib.commands.discord.Command;
import com.danielm59.botlib.commands.discord.CommandCategory;
import com.danielm59.botlib.commands.discord.DiscordCommandRegistry;
import com.danielm59.botlib.commands.discord.DiscordCommands;
import com.danielm59.botlib.commands.discord.Permissions;
import com.danielm59.botlib.discord.Emoji;
import com.danielm59.botlib.games.trivia.api.Questions.Question;
import com.danielm59.botlib.games.trivia.api.TriviaAPI;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.MessageChannel;
import discord4j.core.object.entity.User;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.core.spec.EmbedCreateSpec;
import org.apache.commons.text.StringEscapeUtils;
import reactor.core.publisher.Mono;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TriviaGame implements DiscordCommandRegistry
{
    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private static HashMap<String, Color> difficultyColor = new LinkedHashMap<>();

    static
    {
        difficultyColor.put("easy", new Color(0, 255, 0));
        difficultyColor.put("medium", new Color(255, 255, 0));
        difficultyColor.put("hard", new Color(255, 0, 0));
    }

    public Mono<Void> trivia(MessageCreateEvent event)
    {
        try
        {
            Question question = TriviaAPI.getQuestion();
            Consumer<EmbedCreateSpec> embed = spec ->
            {
                spec.setTitle(parseText(question.getQuestion()));
                spec.setColor(difficultyColor.get(question.getDifficulty()));
            };

            switch (question.getType())
            {
                case "multiple":
                    multiQuestion(embed, question, event.getMessage().getChannel().block());
                    break;
                case "boolean":
                    boolQuestion(embed, question, event.getMessage().getChannel().block());
                    break;
                default:
                    throw new IllegalArgumentException(question.getType());
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return Mono.empty().then();
    }

    private void multiQuestion(Consumer<EmbedCreateSpec> embed, Question question, MessageChannel channel)
    {
        List<String> answers = question.getIncorrectAnswers();
        answers.add(question.getCorrectAnswer());
        Collections.shuffle(answers);
        StringBuilder s = new StringBuilder();
        s.append(Emoji.A.getRaw()).append(parseText(answers.get(0))).append("\n");
        s.append(Emoji.B.getRaw()).append(parseText(answers.get(1))).append("\n");
        s.append(Emoji.C.getRaw()).append(parseText(answers.get(2))).append("\n");
        s.append(Emoji.D.getRaw()).append(parseText(answers.get(3)));
        Consumer<EmbedCreateSpec> finalEmbed = embed.andThen(spec -> spec.setDescription(s.toString()));
        Message message = channel.createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(finalEmbed)).block();
        scheduler.schedule(() -> multiReactions(message), 1L, TimeUnit.SECONDS);
        scheduler.schedule(() -> multiAnswer(message, question, answers), 60L, TimeUnit.SECONDS);
    }

    private void boolQuestion(Consumer<EmbedCreateSpec> embed, Question question, MessageChannel channel)
    {
        StringBuilder s = new StringBuilder();
        s.append(Emoji.TRUE.getRaw()).append("True").append("\n");
        s.append(Emoji.FALSE.getRaw()).append("False");
        Consumer<EmbedCreateSpec> finalEmbed = embed.andThen(spec -> spec.setDescription(s.toString()));
        Message message = channel.createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(finalEmbed)).block();
        scheduler.schedule(() -> boolReactions(message), 1L, TimeUnit.SECONDS);
        scheduler.schedule(() -> boolAnswer(message, question), 60L, TimeUnit.SECONDS);
    }

    private void multiReactions(Message message)
    {
        message.addReaction(Emoji.A).block();
        message.addReaction(Emoji.B).block();
        message.addReaction(Emoji.C).block();
        message.addReaction(Emoji.D).block();
    }

    private void boolReactions(Message message)
    {
        message.addReaction(Emoji.TRUE).block();
        message.addReaction(Emoji.FALSE).block();
    }

    private void multiAnswer(Message message, Question question, List<String> answers)
    {
        MessageChannel channel = message.getChannel().block();
        int correctId = answers.indexOf(question.getCorrectAnswer());
        List<Integer> wrongId = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
        wrongId.remove(correctId);
        ReactionEmoji.Unicode[] reactions = new ReactionEmoji.Unicode[]{Emoji.A, Emoji.B, Emoji.C, Emoji.D};
        List<User> correct = new ArrayList<>();
        List<User> correctList = message.getReactors(reactions[correctId]).collectList().block();
        if (correctList != null)
            correct.addAll(correctList);
        for (int id : wrongId)
        {
            List<User> wrongList = message.getReactors(reactions[id]).collectList().block();
            if (wrongList != null)
                correct.removeAll(wrongList);
        }
        message.delete();
        correctAnswerPost(channel, question, reactions[correctId], answers.get(correctId), correct);
    }

    private void boolAnswer(Message message, Question question)
    {

        MessageChannel channel = message.getChannel().block();
        List<User> correct = new ArrayList<>();
        ReactionEmoji.Unicode correctEmoji;
        if (question.getCorrectAnswer().equals("True"))
        {
            List<User> correctList = message.getReactors(correctEmoji = Emoji.TRUE).collectList().block();
            if (correctList != null)
                correct.addAll(correctList);
            List<User> wrongList = message.getReactors(Emoji.FALSE).collectList().block();
            if (wrongList != null)
                correct.removeAll(wrongList);
        } else
        {
            List<User> correctList = message.getReactors(correctEmoji = Emoji.FALSE).collectList().block();
            if (correctList != null)
                correct.addAll(correctList);
            List<User> wrongList = message.getReactors(Emoji.TRUE).collectList().block();
            if (wrongList != null)
                correct.removeAll(wrongList);
        }
        message.delete().subscribe();
        correctAnswerPost(channel, question, correctEmoji, question.getCorrectAnswer(), correct);
    }

    private void correctAnswerPost(MessageChannel channel, Question question, ReactionEmoji.Unicode emoji, String answer, List<User> correct)
    {
        Consumer<EmbedCreateSpec> embed = spec ->
        {
            spec.setColor(difficultyColor.get(question.getDifficulty()));
            spec.setTitle(parseText(question.getQuestion()));
            StringBuilder s = new StringBuilder();
            s.append(emoji.getRaw()).append(parseText(answer));
            spec.setDescription(s.toString());
            if (correct.size() > 0)
            {
                s = new StringBuilder();
                s.append("Correct: ");
                //TODO we should convert to members and get display names here
                s.append(correct.stream().map(User::getUsername).collect(Collectors.joining(", ")));
                spec.setFooter(s.toString(), "");
            }
        };
        channel.createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(embed)).block();
    }


    private String parseText(String text)
    {
        text = StringEscapeUtils.unescapeHtml4(text);
        text = text.replace("*", "\\*");
        text = text.replace("_", "\\_");
        text = text.replace("~", "\\~");
        text = text.replace("&amp;", "&");

        return text;
    }

    @Override
    public void registerCommands(DiscordCommands commands)
    {
        commands.registerCommand(CommandCategory.GAMES, "!Trivia",new Command(this::trivia, Permissions::general));
    }
}
